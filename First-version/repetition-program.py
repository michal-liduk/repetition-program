import random
from datetime import datetime, date, timedelta
import json
import os
import pandas as pd
from IPython.display import display

class RepetitionProgram:
    def __init__(self):
            self.vocabulary = {}
            self.now = datetime.now()
            self.today = date.today()
            self.user = ""
            self.good_answer = 0
            self.wrong_answer = 0

    def menu(self, *question):
        while question != "exit":
            question = input("Welcome, what do you want to do? Do you have an account? y/n or would you like to 'exit' ")
            if question not in ["y", "n", "exit"]:
                print("You should enter a valid option! Try again!")
                continue
            
            elif question == "exit":
                print("Goodbye!")
                exit()

            elif question == "y":
                self.user = input("Enter your name: ")
                if os.path.isfile(f"./{self.user}_file.json"):
                    with open(f"{self.user}_file.json", "r") as read_file:
                        self.vocabulary = json.load(read_file)
                        for word in self.vocabulary:
                            date_time_str = self.vocabulary[word]["next_repetition"]
                            self.vocabulary[word]["next_repetition"] = datetime.strptime(date_time_str, '%Y-%m-%d %H:%M:%S.%f')
                self.program()

            elif question == "n":
                self.user = input("Enter your name. ")
                self.program()

    def program(self, *question):
        while question != "exit":
            question = input("What do you want to do, press:\n'1' for start_repetition;\n'2' for add new words;\n'3' for remove words;\n'4' to see statistics;\n'exit' in order to exit:\n")
            if question not in ["1", "2", "3", "4", "exit"]:
                print("You should enter a valid option! Try again!")
                continue
            
            elif question == "exit":
                print("Thank you for today learning!\n\nDziękuję za dzisiejszą naukę!")
                with open(f"{self.user}_file.json", "w") as write_file:
                    json.dump(self.vocabulary, write_file, indent=4, default=str)
                print(f"Today: good answers: {self.good_answer}")
                print(f"Today: bad_answers: {self.wrong_answer}")
                exit()
                break

            elif question == "1":
                self.repetitions()
            
            elif question == "2":
                self.add_words()

            elif question == "3":
                self.remove_words()

            elif question == "4":
                self.statistics()

      
    def repetitions(self):
        vocabulary_to_repeat = {}
        for key, value in self.vocabulary.items():
            if value["next_repetition"] <= self.now:
                vocabulary_to_repeat[key] = value
        
        print(vocabulary_to_repeat)
        print("Number of words to repeat: ", len(vocabulary_to_repeat))
        if len(vocabulary_to_repeat) == 0:
            print("You don't have repetitons for today!")
            pass
        else:
            while len(vocabulary_to_repeat) > 0:
                word_to_answer = random.choice(list(vocabulary_to_repeat))
                print(word_to_answer)
                answer = input("Enter the answer: ")
                if self.vocabulary[word_to_answer]["translation"] == answer:
                    print("OK")
                    self.good_answer += 1
                else:
                    print("Wrong!")
                    self.wrong_answer += 1
                    
                your_rate = input("Rate you answer: Wrong - '1', Average - '2', Good - '3'. ")
                if your_rate == "3":
                    del vocabulary_to_repeat[word_to_answer]
                    if self.vocabulary[word_to_answer]["priority"] <= 0:
                        next_repetition = self.now + timedelta(days=3)
                        self.vocabulary[word_to_answer]["next_repetition"] = next_repetition
                        self.vocabulary[word_to_answer]["priority"] += 2

                    elif self.vocabulary[word_to_answer]["priority"] < 5:
                        next_repetition = self.now + timedelta(days=5)
                        self.vocabulary[word_to_answer]["next_repetition"] = next_repetition
                        self.vocabulary[word_to_answer]["priority"] += 5

                    elif self.vocabulary[word_to_answer]["priority"] < 20:
                        next_repetition = self.now + timedelta(days=10)
                        self.vocabulary[word_to_answer]["next_repetition"] = next_repetition
                        self.vocabulary[word_to_answer]["priority"] += 15

                    elif self.vocabulary[word_to_answer]["priority"] < 50:
                        next_repetition = self.now + timedelta(days=20)
                        self.vocabulary[word_to_answer]["next_repetition"] = next_repetition
                        self.vocabulary[word_to_answer]["priority"] += 30

                    elif self.vocabulary[word_to_answer]["priority"] < 100:
                        next_repetition = self.now + timedelta(days=50)
                        self.vocabulary[word_to_answer]["next_repetition"] = next_repetition
                        self.vocabulary[word_to_answer]["priority"] += 60

                    elif self.vocabulary[word_to_answer]["priority"] < 300:
                        next_repetition = self.now + timedelta(days=100)
                        self.vocabulary[word_to_answer]["next_repetition"] = next_repetition
                        self.vocabulary[word_to_answer]["priority"] += 150
                    
                    else:
                        next_repetition = self.now + timedelta(days=200)
                        self.vocabulary[word_to_answer]["next_repetition"] = next_repetition
                        self.vocabulary[word_to_answer]["priority"] += 300

                elif your_rate == "2":
                    del vocabulary_to_repeat[word_to_answer]
                    if self.vocabulary[word_to_answer]["priority"] <= 0:
                        next_repetition = self.now + timedelta(days=2)
                        self.vocabulary[word_to_answer]["next_repetition"] = next_repetition
                        self.vocabulary[word_to_answer]["priority"] += 1

                    elif self.vocabulary[word_to_answer]["priority"] < 5:
                        next_repetition = self.now + timedelta(days=3)
                        self.vocabulary[word_to_answer]["next_repetition"] = next_repetition
                        self.vocabulary[word_to_answer]["priority"] += 3

                    elif self.vocabulary[word_to_answer]["priority"] < 20:
                        next_repetition = self.now + timedelta(days=5)
                        self.vocabulary[word_to_answer]["next_repetition"] = next_repetition
                        self.vocabulary[word_to_answer]["priority"] += 10

                    elif self.vocabulary[word_to_answer]["priority"] < 50:
                        next_repetition = self.now + timedelta(days=10)
                        self.vocabulary[word_to_answer]["next_repetition"] = next_repetition
                        self.vocabulary[word_to_answer]["priority"] += 15

                    elif self.vocabulary[word_to_answer]["priority"] < 100:
                        next_repetition = self.now + timedelta(days=25)
                        self.vocabulary[word_to_answer]["next_repetition"] = next_repetition
                        self.vocabulary[word_to_answer]["priority"] += 30

                    elif self.vocabulary[word_to_answer]["priority"] < 300:
                        next_repetition = self.now + timedelta(days=50)
                        self.vocabulary[word_to_answer]["next_repetition"] = next_repetition
                        self.vocabulary[word_to_answer]["priority"] += 75
                  
                    else:
                        next_repetition = self.now + timedelta(days=100)
                        self.vocabulary[word_to_answer]["next_repetition"] = next_repetition
                        self.vocabulary[word_to_answer]["priority"] += 150


                elif your_rate == "1":
                    if self.vocabulary[word_to_answer]["priority"] <= 0:
                        self.vocabulary[word_to_answer]["priority"] -= 1

                    elif self.vocabulary[word_to_answer]["priority"] < 5:
                        self.vocabulary[word_to_answer]["priority"] -= 3

                    elif self.vocabulary[word_to_answer]["priority"] < 20:
                        self.vocabulary[word_to_answer]["priority"] -= 10

                    elif self.vocabulary[word_to_answer]["priority"] < 50:
                        self.vocabulary[word_to_answer]["priority"] -= 15

                    elif self.vocabulary[word_to_answer]["priority"] < 100:
                        self.vocabulary[word_to_answer]["priority"] -= 30

                    elif self.vocabulary[word_to_answer]["priority"] < 300:
                        self.vocabulary[word_to_answer]["priority"] -= 75
                  
                    else:
                        self.vocabulary[word_to_answer]["priority"] -= 150


    def add_words(self, *options):
        while options != "exit":
            options = input("Enter '1' to continue or 'exit' in order to back to the previous menu: ")
            if options not in ["1", "exit"]:
                print("You should enter a valid option! Try again!")
                continue
            elif options == "exit":
                break
            else:
                word = input("Enter the word in polish: ")
                translation = input("Enter the word in english: ")
                if options not in ["1", "exit"]:
                    print("You should enter a valid option! Try again!")
                    continue
                if word in self.vocabulary:
                    print(f'Word {word} is already in vocabulary, pick another')
                else:
                    self.vocabulary[word] = {'translation': translation, 'next_repetition': self.now, 'priority': 0}
                    print(self.vocabulary)

    def remove_words(self, *options):
        while options != "exit":
            word = input("Which word do you want to delete? ")
            try:
                del self.vocabulary[word]
            except KeyError:
                print(f'Word {word} is not in the vocabulary')
            options = input("Enter '1' to continue or 'exit' in order to back to the previous menu: ")
    
    def statistics(self):
        print("Your vocabulary:")
        print("Less number --> Greater Priority")
        df = pd.DataFrame(self.vocabulary)
        df_2 = df.transpose()
        display(df_2)  


if __name__ == "__main__":
    run = RepetitionProgram()
    run.menu()
