import random
from datetime import datetime, date, timedelta
import pandas as pd
from IPython.display import display
import psycopg2
import psycopg2.extras
from loguru import logger


class RepetitionProgram:
    def __init__(self):
            self.vocabulary = {}
            self.now = datetime.now()
            self.today = date.today()
            self.user = ""
            self.good_answer = 0
            self.wrong_answer = 0
            self.conn = None


    def connect(self):
        """Connect to a Postgres database."""
        if self.conn is None:
            try:
                self.conn = psycopg2.connect("host=localhost dbname=postgres user=postgres password=postgres")

            except psycopg2.DatabaseError as e:
                logger.error(e)
                raise e
            finally:
                logger.info('Connection opened successfully.')
        
        self.menu()

    def menu(self, *question):
        cur = self.conn.cursor()

        while question != "exit":
            question = input("Welcome, what do you want to do? Do you have an account? y/n or would you like to 'exit' ")
            if question not in ["y", "n", "exit"]:
                print("You should enter a valid option! Try again!")
                continue
            
            elif question == "exit":
                print("Goodbye!")
                exit()

            elif question == "y":
                self.user = input("Enter your name: ")
                try:
                    cur.execute(f"SELECT * from public.{self.user}_table")
                    print(f"You have an account {self.user}")
                except Exception:
                    print("You don't have an account, but we are making it for you right now!")
                    self.conn.rollback()
                    self.create_table()

            elif question == "n":
                self.user = input("Enter your name. ")
                self.create_table()

            cur.close()
            self.program()

    def program(self, *question):
        while question != "exit":
            question = input("What do you want to do, press:\n'1' for start_repetition;\n'2' for add new words;\n'3' for remove words;\n'4' to see statistics;\n'exit' in order to exit:\n")
            if question not in ["1", "2", "3", "4", "exit"]:
                print("You should enter a valid option! Try again!")
                continue
            
            elif question == "exit":
                print("Thank you for today learning!\n\nDziękuję za dzisiejszą naukę!")
                print(f"Today: good answers: {self.good_answer}")
                print(f"Today: bad_answers: {self.wrong_answer}")
                exit()
                break

            elif question == "1":
                self.repetitions()
            
            elif question == "2":
                self.add_words()

            elif question == "3":
                self.remove_words()

            elif question == "4":
                self.statistics()

      
    def repetitions(self):
        cur = self.conn.cursor()
        cur.execute(f"SELECT * from {self.user}_table")
        self.vocabulary = cur.fetchall()
        self.conn.commit()
        cur.execute(f"SELECT * FROM {self.user}_table WHERE DATE <= '{self.now}'")
        vocabulary_to_repeat = cur.fetchall()
        self.conn.commit()
        
        print(vocabulary_to_repeat)
        print("Number of words to repeat: ", len(vocabulary_to_repeat))
        if len(vocabulary_to_repeat) == 0:
            print("You don't have repetitons for today!")
            pass
        else:
            while len(vocabulary_to_repeat) > 0:
                word_to_answer = random.choice(vocabulary_to_repeat)
                print(word_to_answer[1])
                answer = input("Enter the answer: ")
                if word_to_answer[2] == answer:
                    print("OK")
                    self.good_answer += 1
                else:
                    print("Wrong!")
                    self.wrong_answer += 1
                    
                your_rate = input("Rate you answer: Wrong - '1', Average - '2', Good - '3'. ")
                if your_rate == "3":
                    vocabulary_to_repeat.remove(word_to_answer)
                    cur.execute(f"SELECT * FROM {self.user}_table WHERE POLISH = %s", (word_to_answer[1],))
                    our_word = cur.fetchone()
                    if word_to_answer[4] <= 0:
                        next_repetition = self.now + timedelta(days=3)
                        cur.execute(f"UPDATE {self.user}_table SET DATE = '{next_repetition}', PRIORITY = PRIORITY + 2 WHERE POLISH = '{word_to_answer[1]}'")

                    elif word_to_answer[4] < 5:
                        next_repetition = self.now + timedelta(days=5)
                        cur.execute(f"UPDATE {self.user}_table SET DATE = '{next_repetition}', PRIORITY = PRIORITY + 5 WHERE POLISH = '{word_to_answer[1]}'")

                    elif word_to_answer[4] < 20:
                        next_repetition = self.now + timedelta(days=10)
                        cur.execute(f"UPDATE {self.user}_table SET DATE = '{next_repetition}', PRIORITY = PRIORITY + 15 WHERE POLISH = '{word_to_answer[1]}'")

                    elif word_to_answer[4] < 50:
                        next_repetition = self.now + timedelta(days=20)
                        cur.execute(f"UPDATE {self.user}_table SET DATE = '{next_repetition}', PRIORITY = PRIORITY + 30 WHERE POLISH = '{word_to_answer[1]}'")

                    elif word_to_answer[4] < 100:
                        next_repetition = self.now + timedelta(days=50)
                        cur.execute(f"UPDATE {self.user}_table SET DATE = '{next_repetition}', PRIORITY = PRIORITY + 60 WHERE POLISH = '{word_to_answer[1]}'")

                    elif word_to_answer[4] < 300:
                        next_repetition = self.now + timedelta(days=100)
                        cur.execute(f"UPDATE {self.user}_table SET DATE = '{next_repetition}', PRIORITY = PRIORITY + 150 WHERE POLISH = '{word_to_answer[1]}'")
                    
                    else:
                        next_repetition = self.now + timedelta(days=200)
                        cur.execute(f"UPDATE {self.user}_table SET DATE = '{next_repetition}', PRIORITY = PRIORITY + 300 WHERE POLISH = '{word_to_answer[1]}'")

                elif your_rate == "2":
                    vocabulary_to_repeat.remove(word_to_answer)
                    cur.execute(f"SELECT * FROM {self.user}_table WHERE POLISH = %s", (word_to_answer[1],))
                    our_word = cur.fetchone()
                    if word_to_answer[4] <= 0:
                        next_repetition = self.now + timedelta(days=2)
                        cur.execute(f"UPDATE {self.user}_table SET DATE = '{next_repetition}', PRIORITY = PRIORITY + 1 WHERE POLISH = '{word_to_answer[1]}'")


                    elif word_to_answer[4] < 5:
                        next_repetition = self.now + timedelta(days=3)
                        cur.execute(f"UPDATE {self.user}_table SET DATE = '{next_repetition}', PRIORITY = PRIORITY + 3 WHERE POLISH = '{word_to_answer[1]}'")

                    elif word_to_answer[4] < 20:
                        next_repetition = self.now + timedelta(days=5)
                        cur.execute(f"UPDATE {self.user}_table SET DATE = '{next_repetition}', PRIORITY = PRIORITY + 10 WHERE POLISH = '{word_to_answer[1]}'")

                    elif word_to_answer[4] < 50:
                        next_repetition = self.now + timedelta(days=10)
                        cur.execute(f"UPDATE {self.user}_table SET DATE = '{next_repetition}', PRIORITY = PRIORITY + 15 WHERE POLISH = '{word_to_answer[1]}'")

                    elif word_to_answer[4] < 100:
                        next_repetition = self.now + timedelta(days=25)
                        cur.execute(f"UPDATE {self.user}_table SET DATE = '{next_repetition}', PRIORITY = PRIORITY + 30 WHERE POLISH = '{word_to_answer[1]}'")

                    elif word_to_answer[4] < 300:
                        next_repetition = self.now + timedelta(days=50)
                        cur.execute(f"UPDATE {self.user}_table SET DATE = '{next_repetition}', PRIORITY = PRIORITY + 75 WHERE POLISH = '{word_to_answer[1]}'")
                  
                    else:
                        next_repetition = self.now + timedelta(days=100)
                        cur.execute(f"UPDATE {self.user}_table SET DATE = '{next_repetition}', PRIORITY = PRIORITY + 150 WHERE POLISH = '{word_to_answer[1]}'")


                elif your_rate == "1":
                    if word_to_answer[4] <= 0:
                        cur.execute(f"UPDATE {self.user}_table SET PRIORITY = PRIORITY - 1 WHERE POLISH = '{word_to_answer[1]}'")

                    elif word_to_answer[4] < 5:
                        cur.execute(f"UPDATE {self.user}_table SET PRIORITY = PRIORITY - 3 WHERE POLISH = '{word_to_answer[1]}'")

                    elif word_to_answer[4] < 20:
                        cur.execute(f"UPDATE {self.user}_table SET PRIORITY = PRIORITY - 10 WHERE POLISH = '{word_to_answer[1]}'")

                    elif word_to_answer[4] < 50:
                        cur.execute(f"UPDATE {self.user}_table SET PRIORITY = PRIORITY - 15 WHERE POLISH = '{word_to_answer[1]}'")

                    elif word_to_answer[4] < 100:
                        cur.execute(f"UPDATE {self.user}_table SET PRIORITY = PRIORITY - 30 WHERE POLISH = '{word_to_answer[1]}'")

                    elif word_to_answer[4] < 300:
                        cur.execute(f"UPDATE {self.user}_table SET PRIORITY = PRIORITY - 75 WHERE POLISH = '{word_to_answer[1]}'")
                  
                    else:
                        cur.execute(f"UPDATE {self.user}_table SET PRIORITY = PRIORITY - 150 WHERE POLISH = '{word_to_answer[1]}'")

    def add_words(self, *options):
        cur = self.conn.cursor()
        cur.execute(f"SELECT * from {self.user}_table")
        self.vocabulary = cur.fetchall()
        self.conn.commit()
        while options != "exit":
            options = input("Enter '1' to continue or 'exit' in order to back to the previous menu: ")
            if options not in ["1", "exit"]:
                print("You should enter a valid option! Try again!")
                continue
            elif options == "exit":
                break
            else:
                word = input("Enter the word in polish: ")
                translation = input("Enter the word in english: ")
                if options not in ["1", "exit"]:
                    print("You should enter a valid option! Try again!")
                    continue
                cur.execute(f"SELECT * FROM {self.user}_table WHERE POLISH = %s", (word,))
                check = cur.fetchone()
                if check != None: 
                    print(f'Word {word} is already in vocabulary, pick another')
                else:
                    cur.execute(f"INSERT INTO {self.user}_table (POLISH, ENGLISH, DATE, PRIORITY) VALUES ('{word}', '{translation}', '{self.now}', '{0}')")
                    self.conn.commit()
                    print(self.vocabulary)
        cur.close()

    def remove_words(self, *options):
        cur = self.conn.cursor()
        cur.execute(f"SELECT * from {self.user}_table")
        self.vocabulary = cur.fetchall()
        self.conn.commit()
        while options != "exit":
            word = input("Which word do you want to delete? ")
            cur.execute(f"SELECT * FROM {self.user}_table WHERE POLISH = %s", (word,))
            check = cur.fetchone()
            if check != None:
                cur.execute(f"DELETE FROM {self.user}_table WHERE POLISH = '{word}'")
                self.conn.commit()
                print(self.vocabulary)
            else:
                print(f'Word {word} is not in the vocabulary')
            options = input("Enter '1' to continue or 'exit' in order to back to the previous menu: ")
        cur.close()
    
    def statistics(self):
        print("Your vocabulary:")
        print("Less number --> Greater Priority")
        cur = self.conn.cursor()
        cur.execute(f"SELECT * from {self.user}_table")
        self.vocabulary = cur.fetchall()
        df = pd.DataFrame(self.vocabulary)
        df.rename(columns={0: "ID", 1: "Polish", 2: "English", 3: "Date and Time", 4: "Priority"}, inplace=True)
        df['Date and Time'] = df['Date and Time'].astype('datetime64[s]')
        display(df)
    
    def create_table(self):
        cur = self.conn.cursor()
        cur.execute(f'''CREATE TABLE {self.user}_table
            (ID         SERIAL      PRIMARY KEY,
            POLISH      TEXT        NOT NULL,
            ENGLISH     TEXT        NOT NULL,
            DATE        TIMESTAMP   NOT NULL,
            PRIORITY    INT         NOT NULL);
            ''')
        cur.close()
        self.conn.commit()


if __name__ == "__main__":
    run = RepetitionProgram()
    run.connect()
